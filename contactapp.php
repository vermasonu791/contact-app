<!DOCTYPE html>
<html lang="en">
<head>
  <title>Contact App</title>
   <!-- Meta tags -->
  <meta charset="utf-8">
  <link rel="icon" href="css/logo.png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Bootstrap CSS and JavaScript-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
   <!--external css file-->
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body class="hm-gradient">
  <!--Add header using include-->
  <?php
  include 'header.php';
  ?>
  <div class="header_image">
    <img src="css/logo.png" alt="logo" class="img-fluid logo mx-auto d-block" height="80" width="80" >
    <h1 id="heading1" style="text-align:center;">List of contacts</h1>
    <p id="description">
      <marquee behaviour="alternate" direction="right">Click on view info page to send the message to selected contacts</marquee></p>
    </div>
    <br>
    <div class="container-fluid" style="margin-bottom:100px;">
    <?php
    //Database connection
    $server="localhost";
    $username="verma";
    $password="hello123";
    $db="contact";
    $conn=mysqli_connect($server,$username,$password,$db);
    //Fetch the list of the contacts from database 
    $sql="SELECT * FROM contactdetails ";
    $run=mysqli_query($conn,$sql);
    while($rows = mysqli_fetch_assoc($run)){
      echo "
      <div class='list-group'>
      <a href='infocontact.php?&contact_id=$rows[id]' class='list-group-item list-group-item-action' id='name1'>$rows[name]
      <button class='btn btn-success view'>View info</button>
      </a>
      </div>
      ";
    }
    ?>
    <!--Add footer using include-->
    <?php
    include 'footer.php';
    ?>
</div>
</body>
</html>
