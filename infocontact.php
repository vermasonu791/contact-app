<!DOCTYPE html>
<html lang="en">
<head>
  <title>Contact details</title>
   <!--Meta tags-->
  <meta charset="utf-8">
  <link rel="icon" href="css/logo.png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--Bootstrap and javascript CDN-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
   <!--external css file-->
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body class="header_image">
  <?php
  include 'header.php';
  ?>
  <!--Database connection-->
  <?php
  $server="localhost";
  $username="verma";
  $password="hello123";
  $db="contact";
  $conn=mysqli_connect($server,$username,$password,$db);
  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
?>
<div class="container-fluid" style="margin-bottom:100px;">
<?php
 //View info of contacts and send message and get contact_id from the url
 if(isset($_GET['contact_id'])){
   $sql="SELECT * FROM contactdetails WHERE id='$_GET[contact_id]'";
   $run=mysqli_query($conn,$sql);
   while($rows=mysqli_fetch_assoc($run)){
     ?>
     <?php
     //Retrive Contact info ferom thr database and render in user interface 
     echo " 
     <div class='row'>
        <div class='col-md-12'>
          <div class='card'>
            <div class='row text2' style='font-size:25px;'>
              <span style='font-weight:bold;'>Name:</span><p class='pp-title'>$rows[name]</p><br>
            </div>
            <div class='row text2' style='font-size:25px;'>
              <span style='font-weight:bold;'>Phone Number:</span><p class='pp-title' style='float:inline-end;'>$rows[phone_number]</p>
            </div>
            <div class='row'>
              <a href='send_message.php?con_id=$_GET[contact_id]'><button type='button' class='btn btn-success'>Send Message</button></a>
            </div>
            <br>
            <br>
            <div class='row'>
              <div class='alert alert-info' style='width: 100%;'>
                <strong>Info!</strong> Click on the button to send a OTP.
              </div>
            <div>
          </div>
        </div>
      </div>
      ";
    }
  }
  ?>
  <!--add footer using include-->
  <?php
  include 'footer.php';
  ?>
</div>
</body>
</html>