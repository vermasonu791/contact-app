<!DOCTYPE html>
<html lang="en">
<head>
  <title>Contact details</title>
  <!--Meta tags-->
  <meta charset="utf-8">
  <!--title icon-->
  <link rel="icon" href="css/logo.png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--External css-->
  <link rel="stylesheet" href="css/style.css">
  <!--bootstrap cdn-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body class="hm-gradient">
    <?php
    include 'header.php';
    ?>
    <div class="container"> 
        <br>
        <?php
        //genrate otp usin mat_rand() function and pass it to url
        $otp = mt_rand(100000,999999);
              echo "
              <br>
              <br>
            <div class='row'>
                <div class='col-md-12'>
                <form id='form1' method='post'>
                <label for='message'>Enter message:</label>
                <input type='text' name='message' id='message' cols='30' rows='4' class='form-control' value='Hi,Your OTP is: $otp'>
                br>
                <a href='send_otp.php?contactid=$_GET[con_id]&otpp=$otp'><button type='button' class='btn btn-primary' name='submit'>Submit</button></a>
                </form>
             </div>
      
        </div>
         ";
        ?>
        </div>
        <!--add footer -->
        <?php
        include 'footer.php';
        ?>
</body>
</html>