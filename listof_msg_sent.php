<!DOCTYPE html>
<html lang="en">
<head>
  <title>Contact details</title>
  <meta charset="utf-8">
  <link rel="icon" href="css/logo.png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
 </head>

<body class="hm-gradient">
<?php
include 'header.php';
?>
<div class="container">
 <br>
 <br>   
<!--Database connection-->
<?php
$server="localhost";
$username="verma";
$password="hello123";
$db="contact";
$conn=mysqli_connect($server,$username,$password,$db);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$sql="SELECT * FROM list_of_msg_sent ORDER BY date_msg_sent DESC LIMIT 10";
$run=mysqli_query($conn,$sql);
?>

<div class="table-responsive-sm">
<table id='table_id' class="table table-hover" style="background-color:#fff !important;box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)">
        <thead>
            <tr>
                <th>Name</th>
                <th>Contact</th>
                <th>OTP</th>
                <th>Date & Time</th>
            </tr>
        </thead>
        <?php
        while($rows=mysqli_fetch_assoc($run)){
            ?>
            <?php
            echo "
            <tbody>
            <tr>
                <td>$rows[contact_name]</td>
                <td>$rows[contact]</td>
                <td>$rows[otp]</td>
                <td>$rows[date_msg_sent]</td>
            </tr>
        </tbody>
        ";
        }
        ?>
        </table>
    <br><br><br>
        </div>
        </div>
        <?php
        include 'footer.php';
        ?>
</body>
</html>
